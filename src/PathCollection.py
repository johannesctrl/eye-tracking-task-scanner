class PathCollection:
    def __init__(self, name, exportsPath, videoPath, gazeTLVPath, pupilRecordingFolder):
        self.name = name
        self.exportsPath = exportsPath
        self.videoPath = videoPath
        self.gazeTLVPath = gazeTLVPath
        self.pupilRecordingFolder = pupilRecordingFolder
