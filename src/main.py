import os
import tkinter as tk
import tkinter.font as font
from tkinter import filedialog, END
import ttClasses
import requests
from dotenv import load_dotenv, find_dotenv

from MainFolder import MainFolder
from ListboxElement import ListboxElement
from TaskTracker import TaskTracker
from TaskTrackerLocalPupilData import TaskTrackerLocalPupilData
from TaskTrackerPupilCoreLive import TaskTrackerPupilCoreLive

load_dotenv(find_dotenv())
LARGE_FONT = ("Helvetica", 12)
font_standard = ("Helvetica", 10)


# classes
class App(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.shared_data = {
            "userId": tk.StringVar(),
            "studyId": tk.StringVar(),
            "httpRequestPostUrl": tk.StringVar(),
            "httpRequestGetUrlCheckConnection": tk.StringVar(),
            "checkL2PConnectionCheckbutton": tk.BooleanVar()
        }
        self.title("Eye Focus Task Tracker")
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        self.fontStyle = font.Font(family='Helvetica')

        self.frames = {}
        for f in (StartPage, LiveWebcam, LivePupilCore, LocalPupilData, Options):
            frame = f(container, self)
            self.frames[f] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, controller):
        frame = self.frames[controller]
        frame.tkraise()


class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        lb_header = tk.Label(self, text="Start Page", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, pady=10, padx=10)
        btn_liveWebcam = tk.Button(self, text="Live Webcam (Debugging)", font=font_standard, padx=10,
                                   pady=10,
                                   fg="white", width=20, bg="#ffcccc",
                                   command=lambda: controller.show_frame(LiveWebcam))
        btn_liveWebcam.grid(row=1, column=0)

        btn_livePupilCore = tk.Button(self, text="Live Pupil Core", font=font_standard, padx=10,
                                      pady=10,
                                      fg="white", width=20, bg="#ffcccc",
                                      command=lambda: controller.show_frame(LivePupilCore))
        btn_livePupilCore.grid(row=2, column=0)

        btn_pupilDataFishEye = tk.Button(self, text="Local Pupil Data", font=font_standard, padx=10,
                                         pady=10,
                                         fg="white", width=20, bg="#ffcccc",
                                         command=lambda: controller.show_frame(LocalPupilData))
        btn_pupilDataFishEye.grid(row=3, column=0)

        btn_options = tk.Button(self, text="Options", font=font_standard, padx=10,
                                pady=10,
                                fg="white", width=20, bg="#ffcccc",
                                command=lambda: controller.show_frame(Options))
        btn_options.grid(row=4, column=0)

        btn_close = tk.Button(self, text="Close", font=font_standard, padx=10, pady=10, width=20, fg="white",
                              bg="#ffcccc",
                              command=lambda: self.quit())
        btn_close.grid(row=99, column=0)


class Options(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # header
        lb_header = tk.Label(self, text="Options", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, columnspan=2, pady=10, padx=10)

        # userId
        lb_userId = tk.Label(self, text="UserId", font=font_standard)
        lb_userId.grid(row=1, column=0, pady=10, padx=10)
        entry_userId = tk.Entry(self, textvariable=controller.shared_data["userId"])
        entry_userId.grid(row=1, column=1, pady=10, padx=10)
        entry_userId.insert(0, os.environ.get("DEFAULT_USER_ID"))

        # studyId
        lb_studyId = tk.Label(self, text="StudyId", font=font_standard)
        lb_studyId.grid(row=2, column=0, pady=10, padx=10)
        entry_studyId = tk.Entry(self, textvariable=controller.shared_data["studyId"])
        entry_studyId.grid(row=2, column=1, pady=10, padx=10)
        entry_studyId.insert(0, os.environ.get("DEFAULT_STUDY_ID"))

        # httpRequestPostUrl
        lb_httpRequestPostUrl = tk.Label(self, text="httpRequestPostUrl", font=font_standard)
        lb_httpRequestPostUrl.grid(row=3, column=0, pady=10, padx=10)
        entry_lb_httpRequestPostUrl = tk.Entry(self, textvariable=controller.shared_data["httpRequestPostUrl"])
        entry_lb_httpRequestPostUrl.grid(row=3, column=1, pady=10, padx=10)
        entry_lb_httpRequestPostUrl.insert(0, os.environ.get("DEFAULT_POST_URL"))

        # httpRequestGetUrlCheckConnection
        lb_httpRequestGetUrlCheckConnection = tk.Label(self, text="httpRequestGetUrlCheckL2PConnectivity",
                                                       font=font_standard)
        lb_httpRequestGetUrlCheckConnection.grid(row=4, column=0, pady=10, padx=10)
        entry_lb_httpRequestGetUrlCheckConnection = tk.Entry(self, textvariable=controller.shared_data[
            "httpRequestGetUrlCheckConnection"])
        entry_lb_httpRequestGetUrlCheckConnection.grid(row=4, column=1, pady=10, padx=10)
        entry_lb_httpRequestGetUrlCheckConnection.insert(0, os.environ.get("DEFAULT_GET_CHECK_CONNECTION_URL"))

        # checkL2PConnection checkbox
        checkButton_checkL2PConnection = tk.Checkbutton(self, text='check L2P Connection before start',
                                                        variable=controller.shared_data[
                                                            "checkL2PConnectionCheckbutton"], command=lambda: print(
                controller.shared_data["checkL2PConnectionCheckbutton"].get()), onvalue=1, offvalue=0)
        checkButton_checkL2PConnection.grid(row=5, column=0, columnspan=2, pady=10, padx=10)

        # checkL2PConnection button
        btn_checkL2PConnection = tk.Button(self, text="CheckL2PConnection", font=font_standard, padx=10,
                                           pady=10,
                                           fg="white", width=20, bg="#ffcccc",
                                           command=lambda: check_las2peer_connection(
                                               url=controller.shared_data["httpRequestGetUrlCheckConnection"].get()
                                           ))
        btn_checkL2PConnection.grid(row=6, column=0, columnspan=2, pady=10, padx=10)

        # back
        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, fg="white", bg="#ffcccc",
                             width=20, command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0, columnspan=2)


class LiveWebcam(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        lb_header = tk.Label(self, text="Live Webcam (Debugging)", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, columnspan=2, pady=10, padx=10)

        lb_camIndex = tk.Label(self, text="Camera Index", font=font_standard)
        lb_camIndex.grid(row=1, column=0, pady=10, padx=10)
        camIndex = tk.IntVar()
        camIndex.set(0)
        entry_camIndex = tk.Entry(self, text=camIndex)
        entry_camIndex.grid(row=1, column=1, pady=10, padx=10)

        lb_windowSize = tk.Label(self, text="Window Size (in %)", font=font_standard)
        lb_windowSize.grid(row=2, column=0, pady=10, padx=10)
        windowSize = tk.IntVar()
        windowSize.set(20)  # default value
        entry_windowSize = tk.Entry(self, text=windowSize)
        entry_windowSize.grid(row=2, column=1, pady=10, padx=10)

        xRes = tk.StringVar()
        xRes.set(1920)  # default value
        entry_xRes = tk.Entry(self, text=xRes)
        entry_xRes.grid(row=3, column=1)
        yRes = tk.StringVar()
        yRes.set(1080)  # default value
        entry_yRes = tk.Entry(self, text=yRes)
        entry_yRes.grid(row=4, column=1)
        videoRes = (int(xRes.get()), int(yRes.get()))

        lb_xRes = tk.Label(self, text="x", font=font_standard)
        lb_xRes.grid(row=3, column=0, pady=10, padx=10)
        lb_yRes = tk.Label(self, text="y", font=font_standard)
        lb_yRes.grid(row=4, column=0, pady=10, padx=10)

        btn_startScanning = tk.Button(self, text="Start Live Webcam", font=font_standard, padx=10, pady=10,
                                      fg="white",
                                      width=20, bg="#ffcccc",
                                      command=
                                      lambda: run_routine(
                                          lambda: TaskTracker(camIndex.get(), videoRes, windowSize.get(),
                                                              controller.shared_data["userId"].get(),
                                                              controller.shared_data[
                                                                  "httpRequestPostUrl"].get()).run_eye_tracking_in_threads(),
                                          controller.shared_data["checkL2PConnectionCheckbutton"].get(),
                                          controller.shared_data["httpRequestGetUrlCheckConnection"].get()
                                      ))

        btn_startScanning.grid(row=98, column=0, columnspan=2)

        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, width=20, fg="white",
                             bg="#ffcccc",
                             command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0, columnspan=2)


class LivePupilCore(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        lb_header = tk.Label(self, text="Live Pupil Core", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, columnspan=2, pady=10, padx=10)

        lb_windowSize = tk.Label(self, text="Window Size (in %)", font=font_standard)
        lb_windowSize.grid(row=2, column=0, pady=10, padx=10)
        window_size = tk.IntVar()
        window_size.set(20)  # default value
        entry_windowSize = tk.Entry(self, text=window_size)
        entry_windowSize.grid(row=2, column=1, pady=10, padx=10)

        x_res = tk.StringVar()
        x_res.set(1920)  # default value
        entry_x_res = tk.Entry(self, text=x_res)
        entry_x_res.grid(row=3, column=1)
        y_res = tk.StringVar()
        y_res.set(1080)  # default value
        entry_y_res = tk.Entry(self, text=y_res)
        entry_y_res.grid(row=4, column=1)
        videoRes = (int(x_res.get()), int(y_res.get()))

        lb_x_res = tk.Label(self, text="x", font=font_standard)
        lb_x_res.grid(row=3, column=0, pady=10, padx=10)
        lb_y_res = tk.Label(self, text="y", font=font_standard)
        lb_y_res.grid(row=4, column=0, pady=10, padx=10)

        # lens dropdown menu
        lb_lens = tk.Label(self, text="Lens", font=font_standard)
        lb_lens.grid(row=5, column=0, pady=10, padx=10)
        option_list = [
            "narrow",
            "wide",
        ]
        var_opt_lens = tk.StringVar(self)
        var_opt_lens.set(option_list[0])
        opt_lens = tk.OptionMenu(self, var_opt_lens, *option_list)
        opt_lens.grid(row=5, column=1, pady=10, padx=10)

        btn_start_scanning = tk.Button(self, text="Start scanning", font=font_standard, padx=10, pady=10,
                                       fg="white",
                                       width=20, bg="#ffcccc",
                                       command=
                                       lambda: run_routine(
                                           lambda: TaskTrackerPupilCoreLive(
                                               window_size=window_size.get(),
                                               user_id=controller.shared_data["userId"].get(),
                                               frame_format="bgr",
                                               ip_address="127.0.0.1",
                                               port_number="50020",
                                               http_request_post_url=controller.shared_data["httpRequestPostUrl"].get(),
                                               lens_type=var_opt_lens.get()
                                           ).run_eye_tracking_in_threads(),
                                           controller.shared_data["checkL2PConnectionCheckbutton"].get(),
                                           controller.shared_data["httpRequestGetUrlCheckConnection"].get()))
        btn_start_scanning.grid(row=98, column=0, columnspan=2)

        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, width=20, fg="white",
                             bg="#ffcccc",
                             command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0, columnspan=2)


class LocalPupilData(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # header
        lb_header = tk.Label(self, text="Local Pupil Data", font=LARGE_FONT)
        lb_header.grid(row=0, column=0, columnspan=2)

        # folder picker
        btn_folderPicker = tk.Button(self, text="Pick Folder", padx=10, pady=10, fg="white", width=20, bg="#ffcccc",
                                     command=lambda: self.path_collection_list == self.fill_listbox_with_recording_list(
                                         self.pick_folder(), listbox, var_opt_lens.get()))
        """
        btn_folderPicker = tk.Button(self, text="Pick Folder", padx=10, pady=10, fg="white", width=20, bg="#ffcccc",
                                     command=lambda: self.path_collection_list == self.fill_listbox_with_recording_options(
                                         self.pick_folder(), listbox, var_opt_lens.get()))
        """
        btn_folderPicker.grid(row=1, column=0, columnspan=2)

        # listbox of recordings
        listbox = tk.Listbox(self, selectmode=tk.MULTIPLE)
        listbox.grid(row=2, column=0, columnspan=2)
        listbox.config(width=0)

        # lens dropdown menu
        lb_lens = tk.Label(self, text="Lens Angle", font=font_standard)
        lb_lens.grid(row=3, column=0, pady=10, padx=10)
        OptionList = [
            "narrow",
            "wide",
        ]
        var_opt_lens = tk.StringVar(self)
        var_opt_lens.set(OptionList[0])
        opt_lens = tk.OptionMenu(self, var_opt_lens, *OptionList)
        opt_lens.grid(row=3, column=1, pady=10, padx=10)

        # start scanning
        btn_startScanning = tk.Button(self, text="Start Scanning", padx=10, pady=10, fg="white", width=20,
                                      bg="#ffcccc",
                                      command=lambda: self.start_tracking(listbox, controller))
        btn_startScanning.grid(row=4, column=0, columnspan=2)

        btn_back = tk.Button(self, text="Back", font=font_standard, padx=10, pady=10, width=20, fg="white",
                             bg="#ffcccc",
                             command=lambda: controller.show_frame(StartPage))
        btn_back.grid(row=99, column=0, columnspan=2)

        # global Variables
        self.path_collection_list = None
        self.recording_list = []
        self.listbox_element_list = []

    def pick_folder(self):
        """ Opens a dialog for the user who can pick a folder. Method returns the path of the picked folder. """
        folderPath = tk.StringVar()
        folder_selected = filedialog.askdirectory()
        folderPath.set(folder_selected)
        return folderPath.get() + "/"

    def fill_listbox_with_recording_options(self, main_folder_path, listbox):
        """ Searches for all recordings in the recordings folder and inserts them into a listbox. """
        main_folder = MainFolder(path=main_folder_path)
        self.path_collection_list = main_folder.create_path_collection_list()

        # clear listbox before insert
        listbox.delete(0, END)
        self.listbox_element_list = []

        for path_collection in self.path_collection_list:
            listbox.insert(END, path_collection.name)

    def fill_listbox_with_recording_list(self, main_folder_path, listbox, lens_type):
        """ Searches for all recordings in the recordings folder and inserts them into a listbox. """
        main_folder = MainFolder(path=main_folder_path)

        # clear listbox before insert
        listbox.delete(0, END)
        self.listbox_element_list = []

        for session_folder in main_folder.session_folder_list:
            for recording_folder in session_folder.recording_folder_list:

                # don't add recording folder with missing files to list
                if not (recording_folder.narrow_lens_valid and lens_type == "narrow") and not (
                        recording_folder.valid and lens_type == "wide"):
                    continue

                # add recording_folder to list
                self.recording_list.append(recording_folder)

                # add listbox_element to list
                name = recording_folder.session_folder.name + "/" + recording_folder.name
                listbox_element = ListboxElement(name, recording_folder, lens_type)
                self.listbox_element_list.append(listbox_element)
                listbox.insert(END, name)

    def start_tracking(self, listbox, controller):
        """ Starts tracking for all recordings in a listbox which have been highlighted by the user. """
        print("start_scanning")
        for selection_index in listbox.curselection():
            listbox_element = self.listbox_element_list[selection_index]
            userId = controller.shared_data["userId"].get()
            httpRequestPostUrl = controller.shared_data["httpRequestPostUrl"].get()
            task_tracker_local_pupil_data = TaskTrackerLocalPupilData(listbox_element.recording_folder, userId,
                                                                      httpRequestPostUrl, listbox_element.lens_type)
            task_tracker_local_pupil_data.open_local_pupil_data()

    def start(self, listbox):
        print("start_scanning")
        for i in listbox.curselection():
            ep = self.path_collection_list[i].exportsPath
            vp = self.path_collection_list[i].videoPath
            gp = self.path_collection_list[i].gazeTLVPath
            pupilRecordingFolders = self.path_collection_list[i].pupilRecordingFolder
            ttClasses.openPupilDataFishEye(ep, vp, gp, pupilRecordingFolders)


# functions
def run_routine(startFunction, checkL2PConnectionCheckbutton, url):
    print(checkL2PConnectionCheckbutton)
    if checkL2PConnectionCheckbutton:
        if check_las2peer_connection(url):
            startFunction()
        else:
            return
    else:
        startFunction()


def check_las2peer_connection(url):
    print("checkl2pconnection")
    try:
        r = requests.get(url=url, timeout=5.0)
        if r.ok:
            print("Posting Data to L2P was successful.")
            tk.messagebox.showinfo("L2P Connectivity Check", "Connection to L2P was successful.")
            return True
        else:
            print(r)
            print("Posting Data to L2P wasn't successful.")
            tk.messagebox.showinfo("L2P Connectivity Check", "Connection to L2P wasn't successful.")
    except requests.Timeout:
        tk.messagebox.showinfo("L2P Connectivity Check", "Connection Timeout.")
        return False
    except requests.ConnectionError:
        return False


app = App()
app.mainloop()
