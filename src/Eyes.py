class Eyes:
    """ Eyes describes the x and y position of the gaze on an image. """

    def __init__(self):
        self.x = 0
        self.y = 0

    def set_x(self, x):
        self.x = int(x)

    def set_y(self, y):
        self.y = int(y)

    def get_coo(self):
        return self.x, self.y
