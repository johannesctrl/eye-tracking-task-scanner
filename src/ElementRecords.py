from ElementRecord import ElementRecord


class ElementRecords():
    def __init__(self):
        self.list = []

    def add(self, element):
        self.list.append(element)

    def importTaskElementsPerFrame(self, taskElementsPerFrame, frameNumber):
        for tepf in taskElementsPerFrame:
            elementRecordExists = any(tepf.id == taskElementRecord.id for taskElementRecord in self.list)

            # elementRecordExists = True
            if elementRecordExists == False:
                print("Element-Record für", tepf.id, "existiert noch nicht. Lege neuen Record an")
                elementRecord = ElementRecord(tepf.id)
                self.list.append(elementRecord)
            else:
                for elementRecord in self.list:
                    if tepf.focused == True:
                        elementRecord.timeline.append((frameNumber, True))
                        elementRecord.focusedCounter += 1
                    else:
                        elementRecord.timeline.append((frameNumber, False))
                        elementRecord.unfocusedCounter += 1
