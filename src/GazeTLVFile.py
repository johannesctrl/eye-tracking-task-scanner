import csv


class GazeTLVFile:
    def __init__(self, path):
        self.path = path

    def get_column_by_name(self, column_name):
        """
        Get a list of values of the column's name you give into this function.
        """

        def get_match(column_name, header):
            """
            Match the demanded columnName with all header elements to get the index of the demanded header list's item.
            """
            for i, header_element in enumerate(header):
                if str(header[i]) == column_name:
                    return i

        allowed_column_names = ["GazeTimeStamp", "MediaTimeStamp", "MediaFrameIndex", "Gaze3dX", "Gaze3dY", "Gaze3dZ",
                              "Gaze2dX", "Gaze2dY", "PupilDiaLeft", "PupilDiaRight", "Confidence"]
        if column_name not in allowed_column_names:
            raise ValueError("No valid column name selected.")
        column = []
        world_index = -1
        with open(self.path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter="\t")
            header = next(csv_reader)
            i = get_match(column_name, header)
            for line in csv_reader:
                media_frame_index = int(line[2])
                if world_index == media_frame_index:
                    try:
                        next(csv_reader)
                    except:
                        print("End of CSV reached")
                        break
                else:
                    world_index += 1
                    element = float(line[i])
                    column.append(element)
        return column
