import cv2
from matplotlib import pyplot as plt

from TaskRecord import TaskRecord


class TaskRecords:
    """
    In dieser Klasse sollen alle Tasks gespeichert werden, welche innerhalb einer Session gefunden werden.
    Dazu wird die Sammlung an TasksPerFrame importiert und abgeglichen, ob sich in
    der TaskRecords bereits diese Tasks befinden und Daten hinzugefügt.
    """

    def __init__(self):
        self.list = []

    def add(self, task):
        self.list.append(task)

    def printAllTaskRecords(self):
        for record in self.list:
            print(record.toString())

    def importTasksPerFrame(self, tasksPerFrame, frameNumber):
        """
        In dieser Methode werden die "PerFrame"-Daten mit den persistenten Daten abgeglichen. Falls eine neue Aufgabe
        hinzugekommen ist soll auch eine neue Aufgabe, also Task-Instanz, angelegt werden. Falls die Aufgabe schon mal
        erkannt worden ist, dann werden die Werte der Task-Attribute entsprechend geändert, bzw. hochgezählt.
        """
        for tpf in tasksPerFrame.taskList:
            taskRecordExists = any(tpf.id == taskRecord.id for taskRecord in self.list)
            if taskRecordExists == False:  # Record anlegen
                print("Record für", tpf.id, "existiert noch nicht. Lege einen neuen Record an...")
                singleTaskRecord = TaskRecord(tpf.id)
                self.list.append(singleTaskRecord)
            else:  # angelegten Record updaten
                # Elements
                ###################################################
                taskElementsPerFrame = tpf.elements
                for taskRecord in self.list:
                    elementRecords = taskRecord.elementRecords
                    elementRecords.importTaskElementsPerFrame(taskElementsPerFrame, frameNumber)
                ###################################################

                # Tasks
                for taskRecord in self.list:
                    if taskRecord.id == tpf.id:
                        taskRecord.frameCounter += 1
                        taskRecord.timeline.append(1)
                if tpf.focused == False:
                    taskRecord.unfocusedFrameCounter += 1
                elif tpf.focused == True:
                    taskRecord.focusedFrameCounter += 1
                else:
                    taskRecord.timeline.append(0)

    def drawTasks(self, dframe):
        """
        In dieser Methode werden die persistenten Aufgaben (Task-Instanzen) visuell im Videobild dargestellt.
        """
        index = 0
        color = (140, 22, 99)
        for taskRecord in self.list:
            index += 1
            id_short = taskRecord.id[0:5] + "..." + taskRecord.id[len(taskRecord.id) - 5: len(taskRecord.id)]
            string = str(index) + " id=" + str(id_short) + " frameCounter=" + str(
                taskRecord.frameCounter) + " fcFocused=" + str(taskRecord.focusedFrameCounter) + " fcUnfocused=" + str(
                taskRecord.unfocusedFrameCounter)
            for j, elementRecord in enumerate(taskRecord.elementRecords.list):
                string += "| e" + str(j) + ": "
                # string += "fc=" + str(elementRecord.countFocusedFrames())
                # string += ", ufc=" + str(elementRecord.countUnfocusedFrames())
                string += "fc=" + str(elementRecord.focusedCounter)
                string += ", ufc=" + str(elementRecord.unfocusedCounter)
            # print(string)
            cv2.putText(dframe, string, (index, 50 * index), 1, 1.4, color, 2)
